<?php

namespace Drupal\user_logout_confirm\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a confirmation form for logging out user account.
 *
 * @internal
 */
class UserLogoutConfirmForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_logout_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    if ($this->currentUser()->id() && $this->currentUser()->isAuthenticated()) {
      return $this->t('@name, are you sure you want to logout?', ['@name' => ucfirst($this->currentUser()->getDisplayName())]);
    }
    else {
      return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('<front>')->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Confirm');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->messenger()->addMessage($this->t('You have been successfully logged out.'));
    user_logout();
    $form_state->setRedirect('<front>');
  }

}
