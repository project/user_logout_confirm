<?php

namespace Drupal\user_logout_confirm\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * User logout confirm route subscriber.
 *
 * @package Drupal\user_logout_confirm\Routing
 */
class UserLogoutConfirmRouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('user.logout')) {
      $route->setDefault('_controller', '\Drupal\user_logout_confirm\Controller\UserController::logoutConfirm');
    }
  }

}
