# User Logout Confirmation

This module provides confirmation on user logout. Once confirmed, user
logout process will carry on as per usual.

## Requirements

No special requirements.

Leverages the Drupal core user module.

## Install

Install via composer:

```bash
composer require drupal/user_logout_confirm
drush en user_logout_confirm
```

## Configuration

No configuration is necessary. Once module is enabled, user logout will
require confirmation before user is fully logged out.

## Maintainers

* George Anderson [geoanders](https://www.drupal.org/u/geoanders)
* Ishwar Chandra Tiwari [ishwar](https://www.drupal.org/u/ishwar)
* Shashank Kumar [shashank5563](https://www.drupal.org/u/shashank5563)
